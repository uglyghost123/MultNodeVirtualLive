import ray
import time
import numpy as np
import random
import string
from tqdm import tqdm


def search(array, n):
    res = -1
    for i in range(len(array)):
        if array[i] == n:
            res = i
            break


if __name__ == '__main__':
    array = np.random.randint(0, 1000, size=100)
    array = list(array)
    tic = time.time()
    results = [search(array, random.randint(0, 1000)) for x in range(100000)]
    duration = (time.time() - tic)
    print("duration = "+ str(duration))