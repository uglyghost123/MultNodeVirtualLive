# MultNodeVirtualLive

# 安装要求
1. python 版本>= 3.7
2. 按照 PaddleBoBo 配置环境，也可以按照 requirements.txt 配置环境。
3. cuda和cudnn 环境按照 PaddleBoBo要求配置

该测试系统另外两个模块代码参考项目：

中心控制模块：https://gitee.com/uglyghost123/central_controller

推流模块：https://gitee.com/uglyghost123/streaming_module

本项目基于PaddleBoBo https://gitee.com/xiejiehang/PaddleBoBo 视频生成模型
结合网络技术，致力于构建多主机、网络化协同视频生成系统

# 项目运行
worker部署运行  _python app.py_

# 项目测试方法
运行  _python client_test.py_
使用post请求请求中心控制服务器 

![输入图片说明](%E7%B3%BB%E7%BB%9F%E5%9B%BE.png)

# 不同配置电脑生成时间情况
![输入图片说明](figure6.png)