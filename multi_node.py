from Servers.Linux import Linux
# from Servers.Windows import Windows   # 该部分代码存在问题
from arguments import get_args
args = get_args()


class Cluster(object):
    # 主节点服务器IP地址
    master_ip = args.stream_ip   # '10.220.138.108'
    # 多个服务器IP地址 (未来拓展)
    # IPlist = ['10.220.138.108']

    def __init__(self):
        self.host = Linux(self.master_ip, args.stream_username, args.stream_password)

    # 非root账户登录
    def connect(self):
        self.host.connect()

    # 切换root账户登录
    def switch_root(self):
        pattern1 = r'.*(密码).*'
        pattern2 = r'.*(]#).*'
        cmd1 = 'su root'
        cmd2 = args.stream_password
        self.host.send(cmd1, pattern1)
        self.host.send(cmd2, pattern2)

    # 上传文件到指定目录
    def upload_file(self, source_path, dest_path):
        self.host.upload_file(source_path, dest_path)

