import ray
import time
import numpy as np
import random

ray.init(address="10.220.138.108:6379", _node_ip_address="10.220.138.109")


def tiny_work(array, n):
    res = -1
    for i in range(len(array)):
        if array[i] == n:
            res = i
            break
    return res


@ray.remote
def mega_work(array, n, start, end):

    return [tiny_work(array, n) for x in range(start, end)]


array = np.random.randint(0, 1000, size=100)
array = list(array)
start = time.time()

result_ids = []

[result_ids.append(mega_work.remote(array, random.randint(0, 1000), x*1000, (x+1)*1000)) for x in range(100000)]

results = ray.get(result_ids)

print("duration = " + str(time.time() - start))