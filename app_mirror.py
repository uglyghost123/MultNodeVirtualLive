# -*- coding: utf-8 -*-
import utility
from client import Client

from flask import Flask, request, jsonify

from PaddleTools.TTS import TTSExecutor
from ppgan.apps.wav2lip_predictor import Wav2LipPredictor
from multi_node import Cluster

from arguments import get_args

app = Flask(__name__)
app.debug = True


@app.route('/general_video', methods=['post'])
def general_long_video():
    prams = utility.pre_check(request)

    if len(prams['text']) > args.Max_Text_Length:
        utility.scheduler(wav2lip_predictor, TTS, prams, Cluster, Clients)
    else:
        utility.general_video(wav2lip_predictor, TTS, prams, Cluster)

    return jsonify(code=200,
                   msg='ok',
                   data='success')


if __name__ == '__main__':
    args = get_args()

    '''
    # 推流已转移至服务器 ynos  ip：10.220.138.108
    if platform.system() != 'Windows':
        path = args.path_to_push + 'push_streaming.sh'
        Thread(target=utility.push_streaming,
               args=[path]).start()
    else:
        path = args.path_to_push + 'push_streaming.bat'
        Thread(target=utility.push_streaming,
               args=[path]).start()
    '''

    # 连接linux
    Cluster = Cluster()
    Clients = [Client(host='127.0.0.1', port='10101'),
               Client(host='127.0.0.1', port='10100')]
    '''
    Clients = [Client('10.220.138.20', '10100'),
               Client('10.220.138.23', '10100'),
               Client('10.220.138.18', '10100')]
    '''


    # 热加载
    wav2lip_predictor = Wav2LipPredictor(face_det_batch_size=args.face_det_batch_size,
                                         wav2lip_batch_size=args.wav2lip_batch_size,
                                         face_enhancement=True)
    TTS = TTSExecutor(args.TTS_Config)

    app.run(host=args.host, port=10101)
