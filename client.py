import json
import requests


class Client(object):
    # 通过IP, 用户名，密码，超时时间初始化一个远程Linux主机
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.data = {}

    def allocating_task(self, text, index):
        print(index)
        print(self.port)
        url = 'http://' + self.host + ':' + self.port + '/general_video'
        self.data["text"] = text
        self.data["index"] = index

        response = requests.post(url, data=json.dumps(self.data))

        return response

