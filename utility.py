import os
import time
import subprocess
import platform
import re
import json
from flask import jsonify
from threading import Thread

from arguments import get_args
args = get_args()


'''
预先检查
'''


def pre_check(request):
    if not request.data:  # 检测是否有数据
        return jsonify(code=300,
                       msg='ok',
                       data='fail')

    params = request.data.decode('utf-8')
    prams = json.loads(params)

    return prams


'''
推流视频
'''


def push_streaming(path):
    subprocess.Popen(path, shell=True, stdout=subprocess.PIPE)


'''
恢复被修改的视频
'''


def recovery_video():
    command = 'ffmpeg -y -f concat -i ' + args.recovery_list_path + ' -c copy ' + args.stream1_path
    subprocess.call(command, shell=platform.system() != 'Windows')


'''
生成视频基本单元
'''


def change_video(model, TTS, text, index):
    wavFile_path = args.temp_path + 'output' + str(index) + '.wav'
    TTS.run(text=text, output=wavFile_path)
    video_opening_path = args.temp_path + 'opening.mp4'
    video_ending_path = args.temp_path + 'ending.mp4'
    if index == -1:
        model.run(args.input_video_path, wavFile_path, args.stream1_path)
    elif index == 0:
        model.run(args.input_video_path, wavFile_path, video_opening_path)
    else:
        model.run(args.input_video_path, wavFile_path, video_ending_path)
        command = 'ffmpeg -y -i ./Stream/opening.mp4 -i ./Stream/ending.mp4 -filter_complex "[0:0] [0:1] [1:0] [1:1] concat=n=2:v=1:a=1 [v] [a]" -map "[v]" -map "[a]" ./Stream/stream1.mp4'
        subprocess.call(command, shell=platform.system() != 'Windows')
        command = 'ffmpeg -y -f concat -i ' + args.tmp_video_path + ' -c copy ' + video_opening_path
        subprocess.call(command, shell=platform.system() != 'Windows')
    os.remove(wavFile_path)


'''
按固定间隔分割句子
'''


def cut_text(obj, sec):
    return [obj[i:i+sec] for i in range(0, len(obj), sec)]


'''
根据标点符号分割句子
'''


def segmentation_text(text):
    pattern = r'\.|/|;|\'|`|\[|\]|<|>|\?|:|"|\{|\}|\~|!|@|#|\$|%|\^|&|\(|\)|-|=|\_|\+|。|、|；|‘|’|【|】|·|！| |…|（|）'
    out_text = re.split(pattern, text)
    print(out_text)
    while '' in out_text:
        out_text.remove('')
    return out_text


'''
生成短视频（2秒左右）
'''


def general_video(model, TTS, prams, Cluster):
    wavFile_path = args.temp_path + 'output' + prams['index'] + '.wav'
    TTS.run(text=prams['text'], output=wavFile_path)
    model.run(args.input_video_path, wavFile_path, args.stream1_path)
    server_path = args.server_path + prams['index'] + '.mp4'
    print(server_path)
    Cluster.upload_file(args.stream1_path, server_path)


'''
多节点调度算法（未完善待优化）
'''


def scheduler(model, TTS, prams, Cluster, Clients):
    # 可用计算服务器数量
    worker_num = len(Clients)
    # 文本长度
    string_len = len(prams['text'])
    # 单计算服务器需要处理的文本长度
    single_len = string_len / worker_num
    if single_len <= args.Max_Text_Length:
        Text_Length = single_len
    else:
        Text_Length = args.Max_Text_Length

    # 拆分长句
    out_text = cut_text(prams['text'], int(Text_Length) + 1)

    turn = 0            # 任务分配轮次
    file_index = 0      # 已处理视频块数量
    while file_index <= len(out_text) - worker_num:
        for index, client in enumerate(Clients):
            file_index = worker_num * turn + index
            if index == 0:
                prams['index'] = str(file_index)
                Thread(target=general_video, args=(model, TTS, prams, Cluster)).start()
            else:
                Thread(target=client.allocating_task, args=(out_text[file_index], str(file_index))).start()

        turn = turn + 1

