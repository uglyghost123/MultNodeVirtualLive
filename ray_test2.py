'''在 Python 中运行并行任务的代码'''
import ray
import time

# Start Ray.
ray.init(address="10.220.138.108:6379", _node_ip_address="10.220.138.109")


@ray.remote
def f(x):
    time.sleep(1)
    return x


# Start 4 tasks in parallel.
result_ids = []
for i in range(4):
    result_ids.append(f.remote(i))

# Wait for the tasks to complete and retrieve the results.
# With at least 4 cores, this will take 1 second.
results = ray.get(result_ids)  # [0, 1, 2, 3]
print(results)